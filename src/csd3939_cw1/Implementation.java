package csd3939_cw1;

import csd3939_cw1.DataTypes.AlgorithmType;
import csd3939_cw1.GUI.IExecuteAlgorithm;
import csd3939_cw1.GUI.MainGui;
import csd3939_cw1.Utils.ErrorHandling;
import csd3939_cw1.Utils.FileLoader;
import java.io.IOException;

/**
 *
 * @author Mark
 */
public class Implementation implements IExecuteAlgorithm{
    
    public Implementation() {
        //setup the gui
        MainGui.init(this);
    }
    
    /**
     * Method used to execute the selected algorithm
     * @param path
     * @param algorithmType
     * @param multiple
     */
    @Override
    public void execute(String path, AlgorithmType algorithmType, boolean multiple) {
        
        try {
            printDividers(100);
            //print details
            
            //Algorithm Name
            System.out.println(
                    String.format("%s %s%s%s",
                            "Algorithm Name:",
                            "\u001B[34m", algorithmType.toString(), "\u001B[0m"
                    ));
            
            //Multiple Solutions?
            System.out.println(
                    String.format("%s %s%s%s",
                            "Multiple Solutions?:",
                            "\u001B[34m", Boolean.toString(multiple), "\u001B[0m"
                    ));
            
            //path
            System.out.println(String.format("Path: %s%s%s", "\u001B[34m", path, "\u001B[0m"));
            printDividers(100);
            
            //algorithm selector switch case
            switch(algorithmType){
                case DFS_Original:{
                    if(multiple){
                        System.out.println(String.format("%s%s%s", "\u001B[31m", "Warning: The original DFS algorithm can produce only one solution!\nPlease select DFS_Multi for multiple solutions","\u001B[0m"));
                    }
                    csd3939_cw1.DFS.DFS dfs = new csd3939_cw1.DFS.DFS(FileLoader.readTextFile(path));
                    dfs.evaluate();
                    break;
                }
                case DFS_Multi:{
                    csd3939_cw1.DFS_1.DFS dfs = new csd3939_cw1.DFS_1.DFS(FileLoader.readTextFile(path), multiple);
                    dfs.evaluate();
                    break;
                }
                case BFS:{
                    csd3939_cw1.BFS.BFS bfs = new csd3939_cw1.BFS.BFS(FileLoader.readTextFile(path), multiple);
                    bfs.evaluate();
                    break;
                }
                default:{
                    System.out.println(String.format("%s%s%s", "\u001B[31m", "Error: Algorithm NOT supported yet!","\u001B[0m"));
                    break;
                }
            }
            
            printDividers(100);
        } catch (IOException ex) {
            ErrorHandling.output(ex);
        } catch (CloneNotSupportedException ex) {
            ErrorHandling.output(ex);
        }
        
    }
    
    /**
     * method used to print dividers between solutions
     * @param length number of stars
     */
    private void printDividers(int length){
        for(int i=0;i<length;i++){
            System.out.print("\u001B[33m*");
        }
        System.out.println("\u001B[0m");
    }
}
