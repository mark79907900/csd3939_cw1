/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw1.DFS_1;

import csd3939_cw1.DFS.*;
import csd3939_cw1.DataTypes.Point;
import csd3939_cw1.Utils.Profiler;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Mark
 */
public class DFS {
    
    private Buffer buffer;
    private Profiler profiler;
    
    private boolean noSolution;
    private final boolean multipleSolution;
    private double pointA;
    private double pointB;
    private Point point;
    private int level;
    
    public DFS(ArrayList<Point> maze, boolean multiple) {
        this.buffer = new Buffer(maze);
        this.noSolution = false;
        this.profiler = new Profiler(false);
        this.multipleSolution = multiple;
        this.pointA = 0;
        this.pointB = 0;
        this.level = 0;
    }
    
    public void evaluate() throws CloneNotSupportedException{
        this.profiler.start();
        
        //loop until goal node is found
        while(checkGoal()){
            this.point = findPoint(this.point);
            
            //gets pointA to check if it is goal point
            //point.getPointA() is not used directly in the while loop to cater for null values
            if(point != null){
                this.pointA = point.getPointA();
                this.pointB = point.getPointB();
            }
        }
        this.profiler.printDiff();
        
        if(this.noSolution && this.buffer.getNumberOfSolutions() < 1){
            System.out.println("\u001B[34mNo Solution\u001B[0m");
        }
        
//        this.buffer.printFullPath();
        this.buffer.printPath(this.multipleSolution);
    }
    
    /**
     * Method used to check for goal node. Caters also for no solution
     * @return 
     */
    public boolean checkGoal(){
        if(this.multipleSolution){
            if(this.pointA == 1 || this.pointB == 1){
                this.buffer.storePath();
                this.point = null;
            }
            return !this.noSolution;
        }else{
            if(this.pointA == 1 || this.pointB == 1){
                this.buffer.storePath();
            }
            return (!(this.pointA == 1 || this.pointB == 1) && !this.noSolution);
        }
    }
    
    /**
     * Find a point with the passed value
     * @param value
     * @return
     */
    public Point findPoint(Point point) throws CloneNotSupportedException{
        //check if the passed point is not equal to null
        //if it is equals to null pop the last point
        //if there are no points, find a start point
        if(point == null && this.buffer.getPathSize() == 0){
            point = buffer.searchStartPoint();
            //if start point is found set this parameter to pointA as this would be a start point
            if(point == null){
                this.noSolution = true;
            }
            if(buffer.push(point, this.level)){
                this.level += 1;
            }
            return point;
        }
        else if(point == null && this.buffer.getPathSize() != 0){
            point = this.buffer.pop();
            this.buffer.wipeBackTrack(this.level);
            this.level -= 1;
            return point;
        }
        //search for point B in (B or A)
        else if(point.isNextSearchAOrB()){
            point = buffer.searchMazePointB(point);
            if(buffer.push(point, this.level)){
                this.level += 1;
            }
            return point;
        }
        //search for point A in (A or B)
        else{
            point = buffer.searchMazePointA(point);
            if(buffer.push(point, this.level)){
                this.level += 1;
            }
            return point;
        }
    }
}
