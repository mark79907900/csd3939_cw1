/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw1.DFS_1;

import csd3939_cw1.DFS.*;
import csd3939_cw1.DataTypes.Point;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Mark
 */
public class Buffer {
    private LinkedList<Point> path;
    private List<Point> backTrack;
    private final List<Point> maze;
    private int startSearchIndex;
    private final List<LinkedList<Point>> solutions;
    
    public Buffer(ArrayList<Point> maze) {
        this.path = new LinkedList();
        this.solutions = new ArrayList<LinkedList<Point>>();
        this.backTrack = new ArrayList<Point>();
        this.maze = maze;
        this.startSearchIndex = 0;
    }
    
    /**
     * Search for the start point
     * @return
     * @throws CloneNotSupportedException
     */
    public Point searchStartPoint() throws CloneNotSupportedException{
        for(int i=this.startSearchIndex;i<this.maze.size();i++){
            Point p = this.maze.get(i);
            if(p.getPointA() == 0 && !backTrackContains(p)){
                Point p2 = p.setNextSerachAOrBTrue();
                this.startSearchIndex = i;
                return p2;
            }
        }
        return null;
    }
    
    /**
     * search the maze list to match point A with point A or B
     * @param point
     * @return
     * @throws CloneNotSupportedException
     */
    public Point searchMazePointA(Point point) throws CloneNotSupportedException{
        for(Point p : this.maze){
            if(p.getPointA() == point.getPointA() && !backTrackContains(p)){
                Point p2 = p.setNextSerachAOrBTrue();
                return p2;
            }else if(p.getPointB() == point.getPointA() && !backTrackContains(p)){
                Point p2 = p.setNextSerachAOrBFalse();
                return p2;
            }
        }
        return null;
    }
    
    /**
     * Search the maze list to match point B with point A or B
     * @param point
     * @return
     * @throws CloneNotSupportedException
     */
    public Point searchMazePointB(Point point) throws CloneNotSupportedException{
        for(Point p : this.maze){
            if(p.getPointB() == point.getPointB() && !backTrackContains(p)){
                Point p2 = p.setNextSerachAOrBFalse();
                return p2;
            }else if(p.getPointA() == point.getPointB() && !backTrackContains(p)){
                Point p2 = p.setNextSerachAOrBTrue();
                return p2;
            }
        }
        return null;
    }
    
    /**
     * check if the point is present in the back track
     * @param point
     * @return
     */
    public boolean backTrackContains(Point point){
        return this.backTrack.parallelStream()
                .anyMatch(p-> p.equals(point));
    }
    
    /**
     * Wipe the backtrack at any given level. This is used to make sure that when
     * there is a loop, the path which should be traversed is not already flagged
     * as traversed
     * @param level
     */
    public void wipeBackTrack(int level){
        for (Iterator<Point> iter = this.backTrack.listIterator(); iter.hasNext(); ) {
            Point p = iter.next();
            if (p.getLevel()> level) {
                iter.remove();
            }
        }
    }
    
    /**
     * push a point in the path(solution). The point is also registered in the backtrack
     * @param p
     */
    public boolean push(Point p, int level){
        if(p != null){
            p.setLevel(level);
            this.path.add(p);
            this.backTrack.add(p);
            return true;
        }
        return false;
    }
    
    /**
     * remove the last point from the path (solution)
     * @return
     */
    public Point pop(){
        this.path.pollLast();
        return (this.path.size() > 0)? this.path.getLast() : null;
    }
    
    /**
     * get the number of solutions
     * @return
     */
    public int getNumberOfSolutions(){
        return this.solutions.size();
    }
    
    /**
     * store a new path(solution) in the solutions list
     */
    public void storePath(){
        this.solutions.add((LinkedList<Point>) this.path.clone());
    }
    
    /**
     * get the size of the path(solution)
     * @return
     */
    public int getPathSize(){
        return this.path.size();
    }
    
    /**
     * print the full path with both points.
     * Used for debugging to see how the code is jumping from one point to another
     */
    public void printFullPath(){
        if(getNumberOfSolutions()>0){
            this.solutions.stream().map((LinkedList<Point> path) -> {
                return path;
            }).forEach((path) -> {
                path.stream().forEach((p) -> {
                    p.print();
                });
            });
        }
    }
    
    /**
     * Print the path as outputted for the end user
     * @param multipleSolutions
     */
    public void printPath(boolean multipleSolutions){
        
        int index = 0;
        int solutionSize = 9999;
        int bestSolutionCounter = 0;
        
        //check that the solution list is populated
        if(getNumberOfSolutions()>0){
            if(multipleSolutions){
                System.out.print("All Solutions:\n");
            }
            
            //loop in the solutions list to get each an every path (solution)
            for(LinkedList<Point> path : this.solutions){
                
                //check the size of every solution and get the index of best solution
                if(path.size() < solutionSize){
                    index = bestSolutionCounter;
                    solutionSize = path.size();
                }
                
                //print every path (solution)
                printPath(path);
                bestSolutionCounter++;
            }
            
            //if multisolutions print best path
            if(multipleSolutions){
                System.out.println("\nBest Solution:");
                printPath(this.solutions.get(index));
            }
        }
    }
    
    /**
     * Print the path as outputted for the end user
     * @param path
     */
    private void printPath(LinkedList<Point> path){
        double oldValue = 0;
        int counter = 0;
        
        System.out.print("\u001B[34m0->");
        
        for(Point p : path) {
            if(p.getPointA() == oldValue){
                System.out.print(String.format("%.0f", p.getPointB()));
                oldValue = p.getPointB();
            }else if(p.getPointB() == oldValue){
                System.out.print(String.format("%.0f", p.getPointA()));
                oldValue = p.getPointA();
            }
            if(counter < path.size() - 1){
                System.out.print("->");
            }else{
                System.out.println("\u001B[0m");
            }
            counter++;
        }
    }
}
