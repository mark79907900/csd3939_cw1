/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw1.DataTypes;

/**
 *
 * @author Mark
 */
public class Point implements Cloneable{
    private double pointA;
    private double pointB;
    private String ID;
    //true == search for point A in (A or B)
    //false == search for point B in (A or B)
    private boolean nextSearchAOrB;
    private int level;
    
    private boolean visited;
    
    public Point(double pointA, double pointB) {
        this.pointA = pointA;
        this.pointB = pointB;
        //String.format was not used to get more performance as this way there is no need to invoke the string builder
        this.ID = "" + pointA + "," + pointB;
        this.nextSearchAOrB = true;
        this.level = 0;
        this.visited = false;
    }
    
    public Point() {
    }
    
    public double getPointA() {
        return this.pointA;
    }
    
    public void setPointA(double pointA) {
        this.pointA = pointA;
    }
    
    public double getPointB() {
        return this.pointB;
    }
    
    public void setPointB(double pointB) {
        this.pointB = pointB;
    }
    
    public String getID() {
        return this.ID;
    }

    public boolean isNextSearchAOrB() {
        return this.nextSearchAOrB;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited() {
        this.visited = true;
    }
    
    public Point toggleNextSerachAOrB() throws CloneNotSupportedException{
        Point p = this.clone();
        p.nextSearchAOrB = !p.nextSearchAOrB;
        return p;
    }
    
    public Point setNextSerachAOrBTrue() throws CloneNotSupportedException{
        Point p = this.clone();
        p.nextSearchAOrB = true;
        return p;
    }
    
    public Point setNextSerachAOrBFalse() throws CloneNotSupportedException{
        Point p = this.clone();
        p.nextSearchAOrB = false;
        return p;
    }
    
    @Override
    public String toString(){
        return String.format("[%.0f %.0f] %d", pointA, pointB, level);
    }
    
    @Override
    public Point clone() throws CloneNotSupportedException{
        return (Point)super.clone();
    }
    
    public boolean equals(Point p){
        return (p.getID().equals(this.ID))? true : false;
    }
    
    public boolean isExitPoint(){
        return (this.pointA == 1)? true : false;
    }
    
    public void print(){
        System.out.println(toString());
    }
}
