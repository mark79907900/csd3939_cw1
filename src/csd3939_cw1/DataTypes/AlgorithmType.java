package csd3939_cw1.DataTypes;

import java.util.stream.Stream;

/**
 *
 * @author Mark
 */
public enum AlgorithmType {
    DFS_Original,
    DFS_Multi,
    BFS;
    
    
    /**
     * Returns the names of all the types of this enum
     * Implementation could be found on 
     * //http://stackoverflow.com/questions/13783295/getting-all-names-in-an-enum-as-a-string
     * @return 
     */
    public static String[] getNames(){
        return Stream.of(AlgorithmType.values()).map(AlgorithmType::name).toArray(String[]::new);
    }
}
