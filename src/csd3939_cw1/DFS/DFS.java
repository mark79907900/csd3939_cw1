/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw1.DFS;

import csd3939_cw1.DataTypes.Point;
import csd3939_cw1.Utils.Profiler;
import java.util.ArrayList;

/**
 *
 * @author Mark
 */
public class DFS {
    
    private Buffer buffer;
    
    private boolean noSolution;
    
    private Profiler profiler;
    
    public DFS(ArrayList<Point> maze) {
        this.buffer = new Buffer(maze);
        this.noSolution = false;
        this.profiler = new Profiler(false);
    }
    
    public void evaluate() throws CloneNotSupportedException{
        Point point = null;
        double pointA = 0;
        double pointB = 0;
        
        //start the profiler
        profiler.start();
        
        //loop until a goal node is found or there is no possible solution
        while(!(pointA == 1 || pointB == 1) && !this.noSolution){
            point = findPoint(point);
            
            //gets pointA to check if it is goal point
            //point.getPointA() is not used directly in the while loop to cater for null values
            if(point != null){
                pointA = point.getPointA();
                pointB = point.getPointB();
            }
        }
        profiler.printDiff();
        
        if(this.noSolution){
            System.out.println("\u001B[34mNo Solution\u001B[0m");
        }
        
        //this.buffer.printFullPath();
        this.buffer.printPath(this.noSolution);
    }
    
    /**
     * Find a point with the passed value
     * @param value
     * @return
     */
    public Point findPoint(Point point) throws CloneNotSupportedException{
        //check if the passed point is not equal to null
        //if it is equals to null pop the last point
        //if there are no points, find a start point
        if(point == null && this.buffer.getPathSize() == 0){
            point = buffer.searchStartPoint();
            //if start point is found set this parameter to pointA as this would be a start point
            if(point == null){
                this.noSolution = true;
            }
            buffer.push(point);
            return point;
        }
        else if(point == null && this.buffer.getPathSize() != 0){
            point = this.buffer.pop();
            return point;
        }
        //search for point B in (B or A)
        else if(point.isNextSearchAOrB()){
            point = buffer.searchMazePointB(point);
            buffer.push(point);
            return point;
        }
        //search for point A in (A or B)
        else{
            point = buffer.searchMazePointA(point);
            buffer.push(point);
            return point;
        }
    }
}
