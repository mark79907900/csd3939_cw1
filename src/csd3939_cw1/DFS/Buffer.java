/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw1.DFS;

import csd3939_cw1.DataTypes.Point;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Mark
 */
public class Buffer {
    private LinkedList<Point> path;
    private List<Point> backTrack;
    private final List<Point> maze;
    private int startSearchIndex;
    
    public Buffer(ArrayList<Point> maze) {
        this.path = new LinkedList();
        this.backTrack = new ArrayList<Point>();
        this.maze = maze;
        this.startSearchIndex = 0;
    }
    
    /**
     * Search for the start point
     * @return
     * @throws CloneNotSupportedException
     */
    public Point searchStartPoint() throws CloneNotSupportedException{
        //the start search index is used for situations where the start point
        //is searched for several times. This would save the location of the
        //last start point in the list and skip the pervious points thus
        //reducing the number of iterations
        for(int i=this.startSearchIndex;i<this.maze.size();i++){
            Point p = this.maze.get(i);
            if(p.getPointA() == 0 && !backTrackContains(p)){
                Point p2 = p.setNextSerachAOrBTrue();
                this.startSearchIndex = i;
                return p2;
            }
        }
        return null;
    }
    
    /**
     * search the maze list to match point A with point A or B
     * @param point
     * @return
     * @throws CloneNotSupportedException
     */
    public Point searchMazePointA(Point point) throws CloneNotSupportedException{
        for(Point p : this.maze){
            if(p.getPointA() == point.getPointA() && !backTrackContains(p)){
                Point p2 = p.setNextSerachAOrBTrue();
                return p2;
            }else if(p.getPointB() == point.getPointA() && !backTrackContains(p)){
                Point p2 = p.setNextSerachAOrBFalse();
                return p2;
            }
        }
        return null;
    }
    
    /**
     * Search the maze list to match point B with point A or B
     * @param point
     * @return
     * @throws CloneNotSupportedException
     */
    public Point searchMazePointB(Point point) throws CloneNotSupportedException{
        for(Point p : this.maze){
            if(p.getPointB() == point.getPointB() && !backTrackContains(p)){
                Point p2 = p.setNextSerachAOrBFalse();
                return p2;
            }else if(p.getPointA() == point.getPointB() && !backTrackContains(p)){
                Point p2 = p.setNextSerachAOrBTrue();
                return p2;
            }
        }
        return null;
    }
    
    /**
     * check if the point is present in the back track
     * @param point
     * @return
     */
    public boolean backTrackContains(Point point){
        return this.backTrack.parallelStream()
                .anyMatch(p-> p.equals(point));
    }
    
    /**
     * push a point in the path(solution). The point is also registered in the backtrack
     * @param p
     */
    public void push(Point p){
        if(p != null){
            this.path.add(p);
            this.backTrack.add(p);
        }
    }
    
    /**
     * remove the last point from the path (solution)
     * @return
     */
    public Point pop(){
        this.path.pollLast();
        return (this.path.size() > 0)? this.path.getLast() : null;
    }
    
    /**
     * get the size of the path(solution)
     * @return
     */
    public int getPathSize(){
        return this.path.size();
    }
    
    /**
     * print the full path with both points.
     * Used for debugging to see how the code is jumping from one point to another
     */
    public void printFullPath(){
        for(Point p : this.path){
            p.print();
        }
    }
    
    /**
     * Print the path as outputted for the end user
     * @param noSolution
     */
    public void printPath(boolean noSolution){
        double oldValue = 0;
        int counter = 0;
        if(!noSolution){
            System.out.print("\u001B[34m0->");
            
            for(Point p : this.path){
                if(p.getPointA() == oldValue){
                    System.out.print(String.format("%.0f", p.getPointB()));
                    oldValue = p.getPointB();
                }else if(p.getPointB() == oldValue){
                    System.out.print(String.format("%.0f", p.getPointA()));
                    oldValue = p.getPointA();
                }
                if(counter < getPathSize() - 1){
                    System.out.print("->");
                }
                counter++;
            }
            System.out.println("\u001B[0m");
        }
        
    }
}
