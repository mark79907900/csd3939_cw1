/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csd3939_cw1.BFS;

/**
 *
 * @author Mark
 */
public class BFSPoint implements Cloneable{
    private final double child;
    private final double parent;
    private final int level;
    private boolean hasLoop;

    public BFSPoint(double child, double parent, int level) {
        this.child = child;
        this.parent = parent;
        this.level = level;
        this.hasLoop = false;
    }

    public double getChild() {
        return child;
    }

    public double getParent() {
        return parent;
    }

    public int getLevel() {
        return level;
    }

    public boolean hasLoop() {
        return hasLoop;
    }

    public void setHasLoop(boolean hasLoop) {
        this.hasLoop = hasLoop;
    }
    
    @Override
    public String toString(){
        return String.format("Child: %.0f Parent: %.0f Level: %d", this.child, this.parent, this.level);
    }
    
    public void print(){
        System.out.println(toString());
    }
    
    @Override
    public BFSPoint clone() throws CloneNotSupportedException{
        return (BFSPoint)super.clone();
    } 
}
