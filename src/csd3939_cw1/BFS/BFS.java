/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw1.BFS;

import csd3939_cw1.DataTypes.Point;
import csd3939_cw1.Utils.Profiler;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author Mark
 */
public class BFS {
    
    private final boolean multipleSolution;
    private final double goalValue;
    private final double startValue;
    private Profiler profiler;
    
    private boolean noSolution;
    private ArrayList<Point> maze;
    private ArrayList<ArrayList<BFSPoint>> searchSpace;
    private ArrayList<ArrayList<String>> Solutions;
    
    public BFS(ArrayList<Point> maze, boolean multiple) {
        this.goalValue = 1;
        this.startValue = 0;
        this.multipleSolution = multiple;
        
        this.profiler = new Profiler(false);
        this.noSolution = false;
        this.maze = maze;
        this.Solutions = new ArrayList<ArrayList<String>>();
        
    }
    
    public void evaluate() throws CloneNotSupportedException{
        
        //start profiler
        this.profiler.start();
        
        //build the search space
        this.searchSpace = searchSpace(this.startValue);
        
        //if there is a disjoint in the search space, it means that there is no solution
        if(this.searchSpace == null){
            this.noSolution = true;
        }else{
            
            //loop in the solutions list
            for(ArrayList<BFSPoint> list : this.searchSpace){
                
                //loop in every solution
                for(BFSPoint point : list){
                    
                    //when a goal point is found generate its path
                    if(point.getChild() == this.goalValue){
                        addSolution(point, this.searchSpace, null, 0);
                        if(!this.multipleSolution){
                            break;
                        }
                    }
                }
            }
        }
        this.profiler.printDiff();
        
        //another possibility for no solution is not to have the required goal point
        if(this.Solutions.size() == 0 || this.noSolution){
            System.out.println("\u001B[34mNo Solution\u001B[0m");
        }else{
            int index = 0;
            int solutionSize = 9999;
            int counter = 0;
            
            if(this.multipleSolution){
                System.out.println("All Solutions:");
            }
            
            //print the solutions
            for(ArrayList<String> solution : this.Solutions){
                if(solution.size() < solutionSize){
                    index = counter;
                    solutionSize = solution.size();
                }
                printSolution(solution);
                counter++;
            }
            
            if(this.multipleSolution){
                System.out.println("\nBest Solution:");
                printSolution(this.Solutions.get(index));
            }
        }
    }
    
    /**
     * method used to print the solution
     * @param solution
     */
    private void printSolution(ArrayList<String> solution){
        for(int i=solution.size()-1;i>=0;i--){
            if(i != 0){
                System.out.print("\u001B[34m"+solution.get(i)+"->");
            }else{
                System.out.print(solution.get(i)+"\u001B[0m\n");
            }
        }
    }
    
    /**
     * Build the searchSpace from the maze
     * @param startValue is the value which is registered as the start value of the maze
     * @return
     * @throws CloneNotSupportedException
     */
    private ArrayList<ArrayList<BFSPoint>> searchSpace(double startValue) throws CloneNotSupportedException{
        ArrayList<ArrayList<BFSPoint>> searchSpace = new ArrayList<ArrayList<BFSPoint>>();
        int visitedCounter = 0;
        
        //a list which will hold the parent values to look for on the next search
        LinkedList<Double> nextLoop = new LinkedList<Double>();
        nextLoop.add(startValue);
        
        //loop until all of the maze has beed added to the searchSpace
        while(this.maze.size() > visitedCounter){
            
            //a list which will hold the level
            ArrayList<BFSPoint> Level = new ArrayList<BFSPoint>();
            
            LinkedList<Double> levelLoop = new LinkedList<Double>(nextLoop);
            nextLoop.clear();
            
            //loop for all the values in levelLoop for next level
            for(double searchValue :levelLoop){
                
                //loop in the maze
                for(Point p : this.maze){
                    //check if not visited
                    if(!p.isVisited()){
                        
                        //if point a is the search value set it as the parent and point b as the child
                        if(p.getPointA() == searchValue){
                            BFSPoint bfsPoint = new BFSPoint(p.getPointB(), searchValue, searchSpace.size());
                            Level.add(bfsPoint);
                            //add a child which should be searched next
                            nextLoop.add(p.getPointB());
                            //set the point as visited
                            p.setVisited();
                            if(this.multipleSolution){
                                bfsPoint.setHasLoop(checkLoops(p.getPointB(), searchValue, searchSpace));
                            }
                            //add a visted counter to indicate that another value was found
                            visitedCounter++;
                        }
                        //if point b is the search value set it as the parent and point a as the child
                        else if(p.getPointB() == searchValue){
                            BFSPoint bfsPoint = new BFSPoint(p.getPointA(), searchValue, searchSpace.size());
                            Level.add(bfsPoint);
                            //add a child which should be searched next
                            nextLoop.add(p.getPointA());
                            //set the point as visited
                            p.setVisited();
                            if(this.multipleSolution){
                                bfsPoint.setHasLoop(checkLoops(p.getPointA(), searchValue, searchSpace));
                            }
                            //add a visted counter to indicate that another value was found
                            visitedCounter++;
                        }
                    }
                    
                }
            }
            
            //happens when there is a disjoint in the search space
            if(Level.size() == 0){
                this.noSolution = true;
                return null;
            }
            searchSpace.add(Level);
        }
        return searchSpace;
    }
    
    /**
     * check if a loop exists
     * @param level
     * @param child
     * @param parent
     * @return
     */
    private boolean checkLoops(double child, double parent, ArrayList<ArrayList<BFSPoint>> searchSpace){
        
        boolean childFlag = false;
        boolean parentFlag = false;
        
        for(ArrayList<BFSPoint> levelList : searchSpace){
            for(BFSPoint point : levelList){
                if(point.getChild() == child){
                    childFlag = true;
                }
                
                if(point.getChild() == parent){
                    parentFlag = true;
                }
            }
        }
        
        if(childFlag && parentFlag){
            return true;
        }
        return false;
    }
    
    /**
     * Add the solution to the solutions list
     * @param point goal point found in the search space
     * @param searchSpace the search space array list
     * @param solution the solution to continue from
     * @param incLevelOffset this is used to increment the number of levels when recursive operations take place
     * @throws CloneNotSupportedException
     */
    private void addSolution(BFSPoint point, ArrayList<ArrayList<BFSPoint>> searchSpace, ArrayList<String> solution, int incLevelOffset) throws CloneNotSupportedException{
        BFSPoint clonePoint = point.clone();
        
        //this list will hold the solution
        if(solution == null){
            solution = new ArrayList<String>();
            solution.add(""+(int)clonePoint.getChild());
            solution.add(""+(int)clonePoint.getParent());
        }
        
        //traverse the correct path to store it
        while(clonePoint.getParent() != this.startValue){
            //get the pervious level
            int nextLevel = clonePoint.getLevel()-1;
            
            //check if pervious level is is greater then 0 or else null pointer
            if(nextLevel >= 0){
                ArrayList<BFSPoint> parentLevel = searchSpace.get(nextLevel);
                
                //loop in the pervious level to find the parent
                for(BFSPoint parentPoint : parentLevel){
                    if(parentPoint.getChild() == clonePoint.getParent()){
                        
                        //add the parent to the solution
                        solution.add(""+(int)parentPoint.getParent());
                        
                        //set the clonePoint to the parentPoint to iterate till the startValue is found
                        clonePoint = parentPoint;
                    }
                }
            }
            
        }
        this.Solutions.add(solution);
        
        //if multuple solution is enabled check for loops
        if(this.multipleSolution){
            int levelOffset = 1 + incLevelOffset;
            //loop in the solution list to get the solutions
            for(String solutionPoint : solution){
                
                //loop in the search space to get the levels one by one
                for(ArrayList<BFSPoint> level:  searchSpace){
                    
                    //loop in the levels, get all points and keep an index
                    int pointInLevelIndex = 0;
                    for(BFSPoint bfspoint: level){
                        
                        //check if the child of each point is found in the solution and is flagged to have a loop
                        if(bfspoint.getChild() == Double.parseDouble(solutionPoint) && bfspoint.hasLoop()){
                            
                            //clone the solution which has the loop
                            ArrayList<String> newSolution = new ArrayList<String>(solution);
                            
                            //trim and remove the part from where the loop was found
                            newSolution.subList(bfspoint.getLevel()+levelOffset, newSolution.size()).clear();
                            
                            //add the parent to the new solution
                            newSolution.add(""+(int)bfspoint.getParent());
                            
                            //make a deep copy of the searchspace
                            ArrayList<ArrayList<BFSPoint>> newSearchSpace = new ArrayList<ArrayList<BFSPoint>>();
                            for(ArrayList<BFSPoint> levelCopy : searchSpace){
                                ArrayList<BFSPoint> newLevelCopy = new ArrayList<BFSPoint>();
                                for(BFSPoint pointCopy:levelCopy){
                                    newLevelCopy.add(pointCopy.clone());
                                }
                                newSearchSpace.add(newLevelCopy);
                            }
                            
                            //unflag the particular point as the loop has been solved
                            newSearchSpace.get(bfspoint.getLevel()).get(pointInLevelIndex).setHasLoop(false);
                            
                            //recall the add solution and pass the part of the solution and the point from where to continue
                            addSolution(bfspoint, newSearchSpace, newSolution, 1);
                        }
                        pointInLevelIndex++;
                    }
                }
            }
        }
    }
}
