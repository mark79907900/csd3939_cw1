/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw1.Utils;

import com.sun.javafx.fxml.PropertyNotFoundException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

/**
 * This class is responsible with the config file
 * @author Mark
 */
public class Config {
    private static Config config = null;
    private static Properties properties;
    
    public static Config Init(String configDestination){
        if(config == null){
            config = new Config(configDestination);
        }
        return config;
    }
    
    /**
     * Get the Config from the passed destination and load them in the properties object
     * @param configDestination
     */
    private Config(String configDestination){
        properties = new Properties();
        
        try(InputStream inputStream = new FileInputStream(configDestination)){
            properties.load(inputStream);
            inputStream.close();
        }catch(FileNotFoundException ex){
            ErrorHandling.output(ex);
        }catch(Exception ex){
            ErrorHandling.output(ex);
        }
    }
    
    /**
     * Method used to get the value for the passed property name
     * @param property
     * @return
     */
    public static String getProperty(String property){
        String value = "";
        try{
            value = properties.getProperty(property);
        }catch(PropertyNotFoundException e){
            ErrorHandling.output(e);
        }
        return value;
    }
    
    /**
     * Method used to get the INT value for the passed property name
     * @param property
     * @return
     */
    public static int getPropertyInt(String property){
        int value = 0;
        try{
            value = Integer.parseInt(properties.getProperty(property));
        }catch(PropertyNotFoundException | NullPointerException e){
            ErrorHandling.output(e);
        }catch(Exception e){
            ErrorHandling.output(e);
        }
        return value;
    }
    
}
