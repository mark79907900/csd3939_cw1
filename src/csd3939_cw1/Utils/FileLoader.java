/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw1.Utils;

import csd3939_cw1.DataTypes.Point;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Mark
 */
public class FileLoader {
    
    /**
     * This method is used to load the points from the given path.
     * It automatically discards repeated points
     * @param path
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static ArrayList<Point> readTextFile(String path) throws FileNotFoundException, IOException {
        
        //array used to hold the maze points
        ArrayList<Point> pointList = new ArrayList<Point>();
        
        BufferedReader br = new BufferedReader(new FileReader(path));
        
        try {
            String line = br.readLine();
            
            while (line != null) {
                String[] parts = line.split(" ");
                
                //check if the same point already exists
                boolean addToList = true;
                for (Point p : pointList) {
                    if (p.getPointA()== Double.parseDouble(parts[0]) && p.getPointB()== Double.parseDouble(parts[1])) {
                        addToList = false;
                    }
                }
                
                //if the point does not exist
                if (addToList) {
                    pointList.add(
                            new Point(Double.parseDouble(parts[0]),Double.parseDouble(parts[1]))
                    );
                }
                
                line = br.readLine();
            }
        } finally {
            br.close();
        }
        return pointList;
    }
}
