package csd3939_cw1.Utils;

/**
 *
 * @author Mark
 */
public class Profiler {
    private static long startTime;
    
    /**
     * Profiler Constructor
     * The start parameter determines if the timer should start when the object is constructed
     * @param start 
     */
    public Profiler(boolean start) {
        if (start){
            this.startTime = System.nanoTime();
        }
    }
    
    /**
     * Start the timer.
     * This could be used to reset the timer
     */
    public void start(){
        this.startTime = System.nanoTime();
    }
    
    /**
     * get the difference from the start
     */
    public long diff(){
        return System.nanoTime() - this.startTime;
    }
    
    /**
     * print the difference from the start
     */
    public void printDiff(){
        long diff = System.nanoTime() - this.startTime;
        System.out.println(String.format("Duration: \u001B[32m%.10f\u001B[0m ms", diff/1000000.0));
    }
}
