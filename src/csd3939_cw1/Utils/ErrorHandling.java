/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csd3939_cw1.Utils;

/**
 * This Class ensures that all the errors are handled the same, thus having a centralised error handling unit
 * @author Mark
 */
public class ErrorHandling {
    
    /**
     * This method is used to process exceptions
     * @param ex 
     */
    public static void output(Exception ex){
//        System.err.println(ex.toString());
        ex.printStackTrace();
    }
    
    /**
     * This method is used to notify a logic error
     * @param msg 
     */
    public static void notifyError(String msg){
        System.err.println(msg);
    }
    
}
