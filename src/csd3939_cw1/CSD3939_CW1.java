/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csd3939_cw1;

import csd3939_cw1.Utils.Config;
import java.io.IOException;

/**
 *
 * @author Mark
 */
public class CSD3939_CW1{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException{
        //Load the config files
        Config.Init("config.properties");
        
        new Implementation();
    }
    
}
