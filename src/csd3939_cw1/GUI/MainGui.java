package csd3939_cw1.GUI;

import csd3939_cw1.DataTypes.AlgorithmType;
import csd3939_cw1.Utils.Config;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Mark
 */
public class MainGui {
    private static MainGui self = null;
    
    private final MainWindow mw;
    private JPanel top;
    private JPanel bottom;
    
    /**
     * This method is used to init the GUI and ensure that only one instance can be created (Singleton)
     * @param callBack
     */
    public static void init(IExecuteAlgorithm callBack){
        if (self == null){
            self = new MainGui(callBack);
        }
    }
    
    /**
     * private Constructor to build the GUI
     * It creates a new Window and sets up the GUI components by invoking the private method setup
     * @param callBack
     */
    private MainGui(IExecuteAlgorithm callBack) {
        this.mw = MainWindow.getMainWindow(Config.getProperty("mainWindowName"));
        setup(callBack);
    }
    
    /**
     * Creates the Components
     * @param callBack
     */
    private void setup(IExecuteAlgorithm callBack){
        
        //Make the top panel which will hold the algorithm selection
        this.top = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        //label for the combobox
        JLabel label = new JLabel("Select an Algorithm:");
        label.setBorder(BorderFactory.createEmptyBorder(5, 20, 5, 10));
        label.setFont(new Font("Serif", Font.PLAIN, 20));
        
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0.75;
        this.top.add(label, c);
        
        //combobox used to select the algoritm to be used.
        //The list of avialable options is being gathered from the AlgorithmType types
        String[] algorithms = AlgorithmType.getNames();
        JComboBox algorithmsList = new JComboBox(algorithms);
        algorithmsList.setPrototypeDisplayValue("XXXXXXXXXX");
        algorithmsList.setSelectedIndex(0);
        
        //add the combo box to the panel
        c.gridx = 1;
        c.gridy = 0;
        this.top.add(algorithmsList, c);
        
        
        JLabel multipleSolutionsLabel = new JLabel("Multiple Solutions?");
        multipleSolutionsLabel.setBorder(BorderFactory.createEmptyBorder(5, 20, 5, 10));
        multipleSolutionsLabel.setFont(new Font("Serif", Font.PLAIN, 20));
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 0.75;
        this.top.add(multipleSolutionsLabel, c);
        
        JCheckBox multiple = new JCheckBox();
        c.gridx = 1;
        c.gridy = 1;
        this.top.add(multiple, c);
        
        //add the panel to the main window
        this.mw.add(this.top, BorderLayout.NORTH);
        
        //make the bottom panel which will hold the file selection
        this.bottom = new JPanel();
        
        //make the fileChooser
        JFileChooser fileChooser = new JFileChooser();
        
        //make the select button
        JButton selectButton = new JButton(Config.getProperty("runFileButtonName"));
        
        //attach an action listener to the button
        selectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                //set the default directory
                fileChooser.setCurrentDirectory(new File(Config.getProperty("fileChooserDefaultDirectory")));
                
                //check for a valid selection
                if (fileChooser.showOpenDialog(mw) == JFileChooser.APPROVE_OPTION) {
                    
                    //get the file path
                    String filePath = fileChooser.getSelectedFile().getAbsolutePath();
                    
                    //delegate the action
                    if(callBack != null){
                        callBack.execute(
                                filePath,
                                AlgorithmType.valueOf((String)algorithmsList.getSelectedItem()),
                                multiple.isSelected()
                        );
                    }
                }
            }
        });
        
        //add the button to the bottom panel
        bottom.add(selectButton);
        
        //add the panel to the main window
        this.mw.add(this.bottom, BorderLayout.SOUTH);
        
        //set the window visiable
        this.mw.setVisible();
        this.mw.setResizable(false);
    }
    
}
