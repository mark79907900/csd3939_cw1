package csd3939_cw1.GUI;

import csd3939_cw1.DataTypes.AlgorithmType;

/**
 *
 * @author Mark
 */
public interface IExecuteAlgorithm {
    public void execute (String path, AlgorithmType algorithmType, boolean multiple);
}
