\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Acronyms}{2}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Solve First Training Maze}{3}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Describe Algorithm(s) Used}{3}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Depth-First Search(DFS)}{3}{subsection.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1}DFS Multiple solution modification}{3}{subsubsection.3.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Breadth-First Search(BFS)}{3}{subsection.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Quality of Code}{5}{section.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Solve All Three Training Mazes (in both modes)}{7}{section.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Solve All Tests}{8}{section.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Recommended algorithm}{8}{subsection.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2}Result Analysis}{8}{subsection.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3}Test Results Single solution}{9}{subsection.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.4}Test Results Multiple solution}{10}{subsection.6.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Repository}{11}{section.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8}References}{12}{section.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {9}Appendix}{13}{section.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.1}Some Screen Shot Of The Training Results}{13}{subsection.9.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.2}Some Screen Shot Of The Testing Results}{15}{subsection.9.2}
